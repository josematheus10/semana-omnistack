const database = require('../database/connection')

module.exports = {

    async index(request, response) {
        const { page = 1 } = request.query

        const [ count ] = await database('Job').count('id')

        const job = await database('Job')
        .join('company', 'company.id', '=', 'job.company_id')
        .orderBy('id', 'asc')
        .limit(5)
        .offset((page - 1) * 5)
        .select([
            'Job.*',
            "company.name as company_name",
            'company.email as company_email',
            'company.whatsapp as company_whastapp',
            'company.city as company_city',
            'company.uf as company_uf',
        ])

        console.log(count)

        response.header('X-Total-Count', count[''])

        return response.json(job)
    },

    async create(request, response) {

        const { title, description, salary } = request.body
        const company_id = request.headers.authorization

        const id = await database('Job').returning('id').insert({
            company_id, title, description, salary
        })


        return response.json({ id : id[0] })
    },

    async delete(request, response) {

        const { id } = request.params
        const company_id = request.headers.authorization
       
        const job = await database('Job')
        .select('*')
        .where('id', id)
        .first()

        if(job != undefined && job.company_id !== company_id){
            return response.status(401).json({
                error: "Sai daqui malandro!"
            })
        }
        
        await database('Job').where('id', id).delete()

        return response.status(204).send()
    }
    
}