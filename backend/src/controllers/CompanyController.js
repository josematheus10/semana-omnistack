const database = require('../database/connection')
const crypto = require('crypto')

module.exports = {

    async index(request, response) {
    
        const company = await database('company').select('*')
        return response.json(company)
    },

    async create(request, response) {

        const {
            name,
            email,
            whatsapp,
            city,
            uf } = request.body

        const id = crypto.randomBytes(4).toString('HEX')

        await database('company').insert({
            id,
            name,
            email,
            whatsapp,
            city,
            uf
        })

        return response.json({ id })
    }
    
}