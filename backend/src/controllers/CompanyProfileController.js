const database = require('../database/connection')

module.exports = {

    async index(request, response) {

        const company_id = request.headers.authorization
        const job = await database('Job')
        .select('*')
        .where('company_id',company_id )
        return response.json(job)
    },
    
}