const database = require('../database/connection')

module.exports = {

    async index(request, response) {

        const { id } = request.body
        const company = await database('Company')
        .select('name')
        .where('id', id)
        .first()

        if(!company){
            return response.status(400).json({
                error : "No company found whith this ID"
            })
        }

        return response.json(company)
    },
    
}