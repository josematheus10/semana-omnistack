const express = require('express')
const companyController = require('./controllers/CompanyController')
const jobController = require('./controllers/JobController')
const companyProfileController = require('./controllers/CompanyProfileController')
const sessionController = require('./controllers/sessionController')

const routes = express.Router()

routes.post('/sessions', sessionController.index)

routes.get('/company', companyController.index)
routes.post('/company', companyController.create)

routes.get('/job', jobController.index)
routes.post('/job', jobController.create)
routes.delete('/job/:id', jobController.delete)

routes.get('/company_profile', companyProfileController.index)

module.exports = routes