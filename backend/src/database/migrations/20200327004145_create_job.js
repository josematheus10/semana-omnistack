
exports.up = function(knex) {
    return knex.schema.createTable('Job', function(table){
        table.increments()
        table.string('company_id').notNullable()

        table.string('title').notNullable()
        table.string('description').notNullable()
        table.decimal('salary')


        table.foreign('company_id').references('id').inTable('Company')
    })
};

exports.down = function(knex) {
    return knex.schema.dropTable('Job')
};
