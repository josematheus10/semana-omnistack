// Update with your config settings.

module.exports = {

  development: {
    client: 'mssql',
    connection: {
      host : 'localhost',
      user : 'node',
      password : 'node@123',
      database : 'appdb',
      options: {
        encrypt: true,
        enableArithAbort: true
      }
    },
    migrations :{
      tableName: 'Migrations',
      directory:'src/database/migrations'
    },
    debug: true
  },

  staging: {
    client: 'postgresql',
    connection: {
      database: 'my_db',
      user:     'username',
      password: 'password'
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  },

  production: {
    client: 'postgresql',
    connection: {
      database: 'my_db',
      user:     'username',
      password: 'password'
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  }

};
